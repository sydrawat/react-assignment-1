import React, { Component } from 'react';
import './App.css';
import UserInput from './UserInput/UserInput'
import UserOutput from './UserOutput/UserOutput'

class App extends Component {

  state = {
    userName: 'sid'
  }

  //event handler
  inputChangeHandler = (event) => {
    this.setState({
      userName: event.target.value
    });
  }
  render() {
    const { userName } = this.state;
    return (
      <div className="App">
        <h1>This is my react app!</h1>
        <UserInput
          value={userName}
          _onChange={this.inputChangeHandler} />
        <UserOutput _userName="siddharth"></UserOutput>
        <UserOutput _userName={userName}></UserOutput>
      </div>
    );
  }
}

export default App;
