import React from 'react';
import './UserInput.css'

const userInput = (props) => {
    const { _onChange, value } = props;
    return (
        <div className="UserInput">
            <h4>user input component rendered here!</h4>
            <input type="text" onChange={_onChange} value={value} />
        </div>
    );
};

export default userInput;