import React from 'react';
import './UserOutput.css'

const userOutput = (props) => {
    const { _userName } = props;
    return (
        <div className="UserOutput">
            <h4>user output component rendered here!</h4>
            <p>Text by : {_userName} Lorem ipsum dolor, sit amet consectetur adipisicing elit. Veritatis dignissimos, eligendi quisquam, asperiores sequi nemo eveniet minima eos voluptatum eius consequuntur alias repellat mollitia dolores! Expedita optio facere aspernatur quos!</p>
            <p>Lorem ipsum dolor sit amet consectetur, adipisicing elit. Quisquam obcaecati accusantium, nostrum rerum eum quia qui veniam sunt illum aperiam corrupti aspernatur earum temporibus sit. Sapiente accusantium tempora libero ex!</p>
        </div>
    );
};

export default userOutput;